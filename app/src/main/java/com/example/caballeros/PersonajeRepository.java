package com.example.caballeros;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class PersonajeRepository {
    private PersonajeDao personajeDao;
    private LiveData<List<PersonajeRepository>> todosPersonajes;


    public PersonajeRepository (Application aplication){
        PersonajesRoomDB bd= PersonajesRoomDB.obtenerBD(aplication);
        personajeDao = bd.personajeDao();
        todosPersonajes = personajeDao.mostrarTodos();  
    }
}
