package com.example.caballeros;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PersonajeDao {
 @Insert
    void  insertar(PersonajeEntity elpersonaje);

 @Update
    void actualizar(PersonajeEntity elpersonaje);

 @Query("DELETE FROM personajes WHERE id_personaje= :elId")
    void borrarId(int elId);


 @Query("SELECT * FROM personajes WHERE estatus= 'True'")
 LiveData<List<PersonajeEntity>> mostrarTodos();
}
