package com.example.caballeros;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;




    @Entity(tableName = "personajes")
    public class PersonajeEntity {
        @PrimaryKey(autoGenerate = true)
        public int id_personaje;
        private String nombre;
        @ColumnInfo(name = "descripcion")
        private String caracteristicas;
        private int categoria;
        private boolean estatus;

        public PersonajeEntity(int id_personaje, String nombre, String caracteristicas, int categoria, boolean estatus) {
            this.id_personaje = id_personaje;
            this.nombre = nombre;
            this.caracteristicas = caracteristicas;
            this.categoria = categoria;
            this.estatus = estatus;
        }

        public int getId_personaje() {
            return id_personaje;
        }

        public void setId_personaje(int id_personaje) {
            this.id_personaje = id_personaje;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getCaracteristicas() {
            return caracteristicas;
        }

        public void setCaracteristicas(String caracteristicas) {
            this.caracteristicas = caracteristicas;
        }

        public int getCategoria() {
            return categoria;
        }

        public void setCategoria(int categoria) {
            this.categoria = categoria;
        }

        public boolean isEstatus() {
            return estatus;
        }

        public void setEstatus(boolean estatus) {
            this.estatus = estatus;
        }
    }


