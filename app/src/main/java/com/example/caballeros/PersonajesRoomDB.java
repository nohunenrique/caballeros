package com.example.caballeros;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;


@Database(entities = {PersonajeEntity.class}, version = 1,exportSchema = false)
public abstract class PersonajesRoomDB extends RoomDatabase {
    //declaramos el objeto de tipo de dato

    public abstract PersonajeDao personajeDao();

    private static volatile PersonajesRoomDB INSTANCIA;

    public static PersonajesRoomDB obtenerBD(final Context context){
        if (INSTANCIA ==null){
            synchronized (PersonajesRoomDB.class){
                if(INSTANCIA==null){
                    INSTANCIA = Room.databaseBuilder(context.getApplicationContext(),
                            PersonajesRoomDB.class, "caballeros")
                            .build();
                }
            }
        }

        return INSTANCIA;
    }



}
